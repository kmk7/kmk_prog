# Bad Request NodeJS skeleton app

This NodeJS skeleton app was made for the Bad Request competition.

The app initialises a working HTTP server using [Express](https://expressjs.com) and provides basic logging with [Winston](https://github.com/winstonjs/winston).

To take advantage of multi-core systems, the app utilises the [Node Cluster](https://nodejs.org/api/cluster.html) to spawn multiple instances to handle the load.

## How to start
- First, you need to have NodeJS installed. You can get it from [NodeJS.org](https://nodejs.org).
- Run `npm install` to install dependencies.
- Run `npm run server` to start the server. By default it starts on <http://localhost:9876>
  - If you want the app to start on a different port you can do it by running eg. `npm run server 8080`

*Good Luck and Have Fun!*