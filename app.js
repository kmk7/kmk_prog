const cluster = require('express-cluster');
const datetime = require('node-datetime');
const express = require('express');
const winston = require('winston');
const port = process.argv[2] || process.env.npm_package_config_default_port;
const bodyParser = require('body-parser');

cluster(function(worker) {
    const app = express();
    const logger = new (winston.Logger)({
        transports: [
            new (winston.transports.Console)({
                timestamp: function() {
                    return `${datetime.create().format('Y-m-d H:M:S')} [thread-${worker.id}]`;
                }
            })
        ]
    });

    app.use(bodyParser.text({ type: 'text/*' }));

    app.use(function(req, res, next) {
        logger.info(`${req.method} ${req.url} ${req.body}`);
        next();
    });

    app.get('/ping', function(req, res) {
        res.send('pong');
    });

    return app.listen(port, function () {
        logger.info(`App is listening on http://localhost:${port}`);
    });
});
